import { Directive, OnInit, DoCheck } from "@angular/core";
import { MatAutocompleteTrigger } from "@angular/material";

@Directive({ selector: `input[matAutocomplete], textarea[matAutocomplete]` })
export class MatAutocompleteTriggerExtension implements OnInit, DoCheck {
    // private properties
    private get editable(): boolean { return this.matAutocompleteTrigger.autocomplete['editable']; }
    private isInputEvent = false;
    private previousEditable!: boolean;
    private previousValue: any;

    // public methods
    constructor(private matAutocompleteTrigger: MatAutocompleteTrigger) { }
    ngOnInit() {
        const { matAutocompleteTrigger, matAutocompleteTrigger: { _handleInput, _onChange } } = this;
        this.previousEditable = this.editable;
        matAutocompleteTrigger._handleInput = new Proxy(_handleInput, { apply: this._handleInputProxy.bind(this) });
        matAutocompleteTrigger._onChange = new Proxy(_onChange, { apply: this._onChangeProxy.bind(this) });
    }
    ngDoCheck() {
        const { editable, previousEditable, matAutocompleteTrigger } = this;
        if (editable !== previousEditable) {
            this.previousEditable = editable;
            const { activeOption, _element: { nativeElement: { value: textValue } } } = <any>matAutocompleteTrigger;
            if (!activeOption) {
                matAutocompleteTrigger._onChange(editable ? textValue : null);
            }
        }
    }

    // private methods
    private _handleInputProxy(target: Function, thisArg: any, argArray?: any): void {
        this.isInputEvent = true;
        Reflect.apply(target, thisArg, argArray);
        this.isInputEvent = false;
    }

    private _onChangeProxy(target: Function, thisArg: any, argArray?: any): void {
        const { editable, isInputEvent, previousValue } = this;
        const value = editable || !isInputEvent ? argArray[0] : null;
        if (previousValue !== value) {
            Reflect.apply(target, thisArg, [this.previousValue = value]);
        }
    }
}