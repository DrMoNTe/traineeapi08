import { Directive, Input } from "@angular/core";
import { MatAutocomplete } from "@angular/material";

@Directive({ selector: 'mat-autocomplete' })
export class MatAutocompleteExtension {
    @Input()
    get editable(): boolean { return this.autocomplete['editable']; }
    set editable(value: boolean) { this.autocomplete['editable'] = value; }
    constructor(private autocomplete: MatAutocomplete) {
        autocomplete['editable'] = true;
    }
}