import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Interface_Customer } from '../interface/interface_customer';
import { Interface_Opportunity } from '../interface/interface_opportunity';
import { Interface_Presale_Resource } from '../interface/interface_presale_resource';
import { Interface_Product } from '../interface/interface_product';
import { Interface_Sale } from '../interface/interface_sale';
import { Interface_Status } from '../interface/interface_status';
import { Interface_Type } from '../interface/interface_type';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
//import { UserValidator} from '../userValidator';




@Component({
  selector: 'app-manage-data',
  templateUrl: './manage-data.component.html',
  styleUrls: ['./manage-data.component.scss']
})
export class ManageDataComponent implements OnInit {

  validation_messages = {
    'c_name': [
      { type: 'required', message: 'Customer Name is required' }
    ],

  };

  c_myForm: FormGroup;
  op_myForm: FormGroup;
  ps_myForm: FormGroup;
  p_myForm: FormGroup;
  am_myForm: FormGroup;
  s_myForm: FormGroup;
  t_myForm: FormGroup;
  Icustomer: Interface_Customer[];
  Iopportunity: Interface_Opportunity[];
  Ipresale_resource: Interface_Presale_Resource[];
  Iproduct: Interface_Product[];
  Isale: Interface_Sale[];
  Istatus: Interface_Status[];
  Itype: Interface_Type[];

  delayTime = 300;

  c_displayedColumns: string[] = ['c_id', 'c_name', 'c_sname', 'option'];

  op_displayedColumns: string[] = ['op_id', 'op_name', 'option'];

  ps_displayedColumns: string[] = ['ps_id', 'ps_name', 'ps_position', 'ps_name2', 'option'];

  p_displayedColumns: string[] = ['p_id', 'p_name', 'option'];

  am_displayedColumns: string[] = ['am_id', 'am_name', 'am_email', 'option'];

  s_displayedColumns: string[] = ['s_id', 's_name', 'option'];

  t_displayedColumns: string[] = ['t_id', 't_name', 'option'];

  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
  }

  ngOnInit() {
    this.loadingAllData(0);
    this.createForms();
  }

  loadingAllData(index) {

    switch (index) {
      case 0: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readCustomerData().subscribe((Icustomer: Interface_Customer[]) => {
            this.Icustomer = Icustomer;
            console.log(this.Icustomer);
          });
        });

        break;
      }
      case 1: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readOpportunityData().subscribe((Iopportunity: Interface_Opportunity[]) => {
            this.Iopportunity = Iopportunity;
            console.log(this.Iopportunity);
          });
        });
        break;
      }
      case 2: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readPresaleResourceData().subscribe((Ipresale_resource: Interface_Presale_Resource[]) => {
            this.Ipresale_resource = Ipresale_resource;
            console.log(this.Ipresale_resource);
          });
        });
        break;
      }
      case 3: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readProductData().subscribe((Iproduct: Interface_Product[]) => {
            this.Iproduct = Iproduct;
            console.log(this.Iproduct);
          });
        });
        break;
      }
      case 4: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readSaleData().subscribe((Isale: Interface_Sale[]) => {
            this.Isale = Isale;
            console.log(this.Isale);
          });
        });
        break;
      }
      case 5: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readStatusData().subscribe((Istatus: Interface_Status[]) => {
            this.Istatus = Istatus;
            console.log(this.Istatus);
          });
        });
        break;
      }
      case 6: {
        this.delay(this.delayTime).then(any => {
          this.apiService.readTypeData().subscribe((Itype: Interface_Type[]) => {
            this.Itype = Itype;
            console.log(this.Itype);
          });
        });
        break;
      }


    }











  }


  /* loadingAllData() {

    this.delay(this.delayTime).then(any => {
      this.apiService.readCustomerData().subscribe((Icustomer: Interface_Customer[]) => {
        this.Icustomer = Icustomer;
        console.log(this.Icustomer);
      });
    });

    this.delay(this.delayTime).then(any => {
      this.apiService.readOpportunityData().subscribe((Iopportunity: Interface_Opportunity[]) => {
        this.Iopportunity = Iopportunity;
        console.log(this.Iopportunity);
      });
    });

    this.delay(this.delayTime).then(any => {
      this.apiService.readPresaleResourceData().subscribe((Ipresale_resource: Interface_Presale_Resource[]) => {
        this.Ipresale_resource = Ipresale_resource;
        console.log(this.Ipresale_resource);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readProductData().subscribe((Iproduct: Interface_Product[]) => {
        this.Iproduct = Iproduct;
        console.log(this.Iproduct);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readSaleData().subscribe((Isale: Interface_Sale[]) => {
        this.Isale = Isale;
        console.log(this.Isale);
      });
    });

    this.delay(this.delayTime).then(any => {
      this.apiService.readStatusData().subscribe((Istatus: Interface_Status[]) => {
        this.Istatus = Istatus;
        console.log(this.Istatus);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readTypeData().subscribe((Itype: Interface_Type[]) => {
        this.Itype = Itype;
        console.log(this.Itype);
      });
    });

  }
 */
   createForms() {

    //C FORM---------------
    this.c_myForm = this.fb.group({
      c_id: [''],
      c_name: ['', Validators.required],
      c_sname: [''],

    });
    this.c_myForm = this.fb.group({
      c_id: new FormControl({ value: null, disabled: true }),
      c_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      c_sname: new FormControl('', Validators.compose([
        Validators.maxLength(30),
      ])),

    });

    //OP FORM----------------
    this.op_myForm = this.fb.group({
      op_id: [''],
      op_name: ['', Validators.required],

    });
    this.op_myForm = this.fb.group({
      op_id: new FormControl({ value: null, disabled: true }),
      op_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });

    //PS FORM----------------
    this.ps_myForm = this.fb.group({
      ps_id: [''],
      ps_name: ['', Validators.required],
      ps_position: ['', Validators.required],
      ps_name2: ['']

    });
    this.ps_myForm = this.fb.group({
      ps_id: new FormControl({ value: null, disabled: true }),
      ps_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      ps_position: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      ps_name2: new FormControl('', Validators.compose([
        Validators.maxLength(50)
      ]))

    });

    //P FORM----------------
    this.p_myForm = this.fb.group({
      p_id: [''],
      p_name: ['', Validators.required],

    });
    this.p_myForm = this.fb.group({
      p_id: new FormControl({ value: null, disabled: true }),
      p_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });

    //AM FORM----------------
    this.am_myForm = this.fb.group({
      am_id: [''],
      am_name: ['', Validators.required],
      am_email: ['', Validators.required]

    });
    this.am_myForm = this.fb.group({
      am_id: new FormControl({ value: null, disabled: true }),
      am_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      am_email: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });

    //S FORM----------------
    this.s_myForm = this.fb.group({
      s_id: [''],
      s_name: ['', Validators.required]

    });
    this.s_myForm = this.fb.group({
      s_id: new FormControl({ value: null, disabled: true }),
      s_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });

    //T FORM ----------------
    this.t_myForm = this.fb.group({
      t_id: [''],
      t_name: ['', Validators.required]

    });
    this.t_myForm = this.fb.group({
      t_id: new FormControl({ value: null, disabled: true }),
      t_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });



  }

  /* createForms(index) {

    switch (index) {
      case 0: {
        //C FORM---------------
        this.c_myForm = this.fb.group({
          c_id: [''],
          c_name: ['', Validators.required],
          c_sname: [''],

        });
        this.c_myForm = this.fb.group({
          c_id: new FormControl({ value: null, disabled: true }),
          c_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ])),
          c_sname: new FormControl('', Validators.compose([
            Validators.maxLength(30),
          ])),

        });
        break;

      }
      case 1: {
        //OP FORM----------------
        this.op_myForm = this.fb.group({
          op_id: [''],
          op_name: ['', Validators.required],

        });
        this.op_myForm = this.fb.group({
          op_id: new FormControl({ value: null, disabled: true }),
          op_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ]))

        });
        break;
      }
      case 2: {
        //PS FORM----------------
        this.ps_myForm = this.fb.group({
          ps_id: [''],
          ps_name: ['', Validators.required],
          ps_position: ['', Validators.required],
          ps_name2: ['']

        });
        this.ps_myForm = this.fb.group({
          ps_id: new FormControl({ value: null, disabled: true }),
          ps_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ])),
          ps_position: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ])),
          ps_name2: new FormControl('', Validators.compose([
            Validators.maxLength(50)
          ]))

        });
        break;
      }
      case 3: {
        //P FORM----------------
        this.p_myForm = this.fb.group({
          p_id: [''],
          p_name: ['', Validators.required],

        });
        this.p_myForm = this.fb.group({
          p_id: new FormControl({ value: null, disabled: true }),
          p_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ]))

        });
        break;

      }
      case 4: {
        //AM FORM----------------
        this.am_myForm = this.fb.group({
          am_id: [''],
          am_name: ['', Validators.required],
          am_email: ['', Validators.required]

        });
        this.am_myForm = this.fb.group({
          am_id: new FormControl({ value: null, disabled: true }),
          am_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ])),
          am_email: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ]))

        });
        break;

      }
      case 5: {
        //S FORM----------------
        this.s_myForm = this.fb.group({
          s_id: [''],
          s_name: ['', Validators.required]

        });
        this.s_myForm = this.fb.group({
          s_id: new FormControl({ value: null, disabled: true }),
          s_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ]))

        });
        break;
      }
      case 6: {
        //T FORM ----------------
        this.t_myForm = this.fb.group({
          t_id: [''],
          t_name: ['', Validators.required]

        });
        this.t_myForm = this.fb.group({
          t_id: new FormControl({ value: null, disabled: true }),
          t_name: new FormControl('', Validators.compose([
            Validators.maxLength(50),
            Validators.required
          ]))

        });
        break;

      }

    }

  } */

  // CUSTOMER FUNCTION ------------------------------

  createOrUpdateCustomer(form) {

    console.log("FORM", form.value);
    if (form.value.c_id) {
      this.apiService.updateCustomerData(form.value).subscribe((Icustomer: Interface_Customer) => {
        console.log("FORM", form.value);
        console.log("Customer Updated", Icustomer);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createCustomerData(form.value).subscribe((Icustomer: Interface_Customer) => {
        console.log("FORM", form.value);
        console.log("Customer Created", Icustomer);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(0);
    this.createForms();

  }
  selectCustomer(Icustomer: Interface_Customer) {
    this.c_myForm = this.fb.group({
      c_id: new FormControl(Icustomer.c_id),
      c_name: new FormControl(Icustomer.c_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      c_sname: new FormControl(Icustomer.c_sname, Validators.compose([
        Validators.maxLength(30),
      ])),

    });
    console.log("Customer Selected", this.c_myForm.value);
  }

  deleteCustomer(id) {
    this.apiService.deleteCustomerData(id).subscribe((Icustomer: Interface_Customer) => {
      console.log("Customer Deleted", Icustomer);
      this.loadingAllData(0);
      alert('DELETE SUCCESS');
    });

  }


  // OPPORTUNITY FUNCTION -----------------------
  createOrUpdateOpportunity(form) {

    console.log("FORM", form.value);
    if (form.value.op_id) {
      this.apiService.updateOpportunityData(form.value).subscribe((Iopportunity: Interface_Opportunity) => {
        console.log("FORM", form.value);
        console.log("Opportunity Updated", Iopportunity);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createOpportunityData(form.value).subscribe((Iopportunity: Interface_Opportunity) => {
        console.log("FORM", form.value);
        console.log("Opportunity Created", Iopportunity);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(1);
    this.createForms();

  }
  selectOpportunity(Iopportunity: Interface_Opportunity) {
    this.op_myForm = this.fb.group({
      op_id: new FormControl(Iopportunity.op_id),
      op_name: new FormControl(Iopportunity.op_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });
    console.log("Opportunity Selected", this.op_myForm.value);
  }

  deleteOpportunity(id) {
    this.apiService.deleteOpportunityData(id).subscribe((Iopportunity: Interface_Opportunity) => {
      console.log("Opportunity Deleted", Iopportunity);
      alert('DELETE SUCCESS');
      this.loadingAllData(1);
    });

  }

  // PRESALE RESOURCE FUNCTION -----------------------
  createOrUpdatePresaleResource(form) {

    console.log("FORM", form.value);
    if (form.value.ps_id) {
      this.apiService.updatePresaleResourceData(form.value).subscribe((Ipresale_resource: Interface_Presale_Resource) => {
        console.log("FORM", form.value);
        console.log("Presale Resource Updated", Ipresale_resource);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createPresaleResourceData(form.value).subscribe((Ipresale_resource: Interface_Presale_Resource) => {
        console.log("FORM", form.value);
        console.log("Presale Resource Created", Ipresale_resource);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(2);
    this.createForms();

  }
  selectPresaleResource(Ipresale_resource: Interface_Presale_Resource) {
    this.ps_myForm = this.fb.group({
      ps_id: new FormControl(Ipresale_resource.ps_id),
      ps_name: new FormControl(Ipresale_resource.ps_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      ps_position: new FormControl(Ipresale_resource.ps_position, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      ps_name2: new FormControl(Ipresale_resource.ps_name2, Validators.compose([
        Validators.maxLength(50)
      ]))

    });
    console.log("Presale Resource Selected", this.ps_myForm.value);
  }

  deletePresaleResource(id) {
    this.apiService.deletePresaleResourceData(id).subscribe((Ipresale_resource: Interface_Presale_Resource) => {
      console.log("Presale Resource Deleted", Ipresale_resource);
      alert('DELETE SUCCESS');
      this.loadingAllData(2);
    });

  }

  // PRODUCT FUNCTION -----------------------
  createOrUpdateProduct(form) {

    console.log("FORM", form.value);
    if (form.value.p_id) {
      this.apiService.updateProductData(form.value).subscribe((Iproduct: Interface_Product) => {
        console.log("FORM", form.value);
        console.log("Product Updated", Iproduct);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createProductData(form.value).subscribe((Iproduct: Interface_Product) => {
        console.log("FORM", form.value);
        console.log("Product Created", Iproduct);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(3);
    this.createForms();

  }
  selectProduct(Iproduct: Interface_Product) {
    this.p_myForm = this.fb.group({
      p_id: new FormControl(Iproduct.p_id),
      p_name: new FormControl(Iproduct.p_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });
    console.log("Product Selected", this.p_myForm.value);
  }

  deleteProduct(id) {
    this.apiService.deleteProductData(id).subscribe((Iproduct: Interface_Product) => {
      console.log("Product Deleted", Iproduct);
      alert('DELETE SUCCESS');
      this.loadingAllData(3);
    });

  }

  // Sale FUNCTION -----------------------
  createOrUpdateSale(form) {

    console.log("FORM", form.value);
    if (form.value.am_id) {
      this.apiService.updateSaleData(form.value).subscribe((Isale: Interface_Sale) => {
        console.log("FORM", form.value);
        console.log("Sale Updated", Isale);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createSaleData(form.value).subscribe((Isale: Interface_Sale) => {
        console.log("FORM", form.value);
        console.log("Sale Created", Isale);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(4);
    this.createForms();

  }
  selectSale(Isale: Interface_Sale) {
    this.am_myForm = this.fb.group({
      am_id: new FormControl(Isale.am_id),
      am_name: new FormControl(Isale.am_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      am_email: new FormControl(Isale.am_email, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });
    console.log("Sale Selected", this.am_myForm.value);
  }

  deleteSale(id) {
    this.apiService.deleteSaleData(id).subscribe((Isale: Interface_Sale) => {
      console.log("Sale Deleted", Isale);
      alert('DELETE SUCCESS');
      this.loadingAllData(4);
    });

  }

  // Status FUNCTION -----------------------
  createOrUpdateStatus(form) {

    console.log("FORM", form.value);
    if (form.value.s_id) {
      this.apiService.updateStatusData(form.value).subscribe((Istatus: Interface_Status) => {
        console.log("FORM", form.value);
        console.log("Status Updated", Istatus);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createStatusData(form.value).subscribe((Istatus: Interface_Status) => {
        console.log("FORM", form.value);
        console.log("Status Created", Istatus);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(5);
    this.createForms();

  }
  selectStatus(Istatus: Interface_Status) {
    this.s_myForm = this.fb.group({
      s_id: new FormControl(Istatus.s_id),
      s_name: new FormControl(Istatus.s_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });
    console.log("Status Selected", this.s_myForm.value);
  }

  deleteStatus(id) {
    this.apiService.deleteStatusData(id).subscribe((Istatus: Interface_Status) => {
      console.log("Status Deleted", Istatus);
      alert('DELETE SUCCESS');
      this.loadingAllData(5);
    });

  }

  // Type FUNCTION -----------------------
  createOrUpdateType(form) {

    console.log("FORM", form.value);
    if (form.value.t_id) {
      this.apiService.updateTypeData(form.value).subscribe((Itype: Interface_Type) => {
        console.log("FORM", form.value);
        console.log("Type Updated", Itype);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createTypeData(form.value).subscribe((Itype: Interface_Type) => {
        console.log("FORM", form.value);
        console.log("Type Created", Itype);
        alert('CREATE SUCCESS');
      });
    }
    this.loadingAllData(6);
    this.createForms();

  }
  selectType(Itype: Interface_Type) {
    this.t_myForm = this.fb.group({
      t_id: new FormControl(Itype.t_id),
      t_name: new FormControl(Itype.t_name, Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))

    });
    console.log("Type Selected", this.t_myForm.value);
  }

  deleteType(id) {
    this.apiService.deleteTypeData(id).subscribe((Itype: Interface_Type) => {
      console.log("Type Deleted", Itype);
      alert('DELETE SUCCESS');
      this.loadingAllData(6);
    });

  }

}


