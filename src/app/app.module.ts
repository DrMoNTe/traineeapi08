import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiService } from '../services/api.service';
import { ReactiveFormsModule } from '@angular/forms';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';


import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { ManageDataComponent } from './manage-data/manage-data.component';
import { MatExpansionModule } from '@angular/material/expansion';
//import {MatTableDataSource} from '@angular/material';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CustomerDialogComponent } from './dialog/customer-dialog/customer-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { OpportunityDialogComponent } from './dialog/opportunity-dialog/opportunity-dialog.component';
import { StatusDialogComponent } from './dialog/status-dialog/status-dialog.component';
import { PresaleResourceDialogComponent } from './dialog/presale-resource-dialog/presale-resource-dialog.component';
import { SaleDialogComponent } from './dialog/sale-dialog/sale-dialog.component';
import { ProductDialogComponent } from './dialog/product-dialog/product-dialog.component';

import { MatAutocompleteExtension } from './../extension/mat-autocomplete.extension';
import { MatAutocompleteTriggerExtension } from './../extension/mat-autocomplete-trigger.extension';

import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ManageDataComponent,
    CustomerDialogComponent,
    OpportunityDialogComponent,
    StatusDialogComponent,
    PresaleResourceDialogComponent,
    SaleDialogComponent,
    ProductDialogComponent,
    MatAutocompleteExtension,
    MatAutocompleteTriggerExtension


  ],
  entryComponents: [
    CustomerDialogComponent,
    OpportunityDialogComponent,
    StatusDialogComponent,
    PresaleResourceDialogComponent,
    SaleDialogComponent,
    ProductDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatGridListModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    //MatTableDataSource,
    MatAutocompleteModule,
    MatTabsModule,
    MatDialogModule,
    MatExpansionModule,
    MatTooltipModule
  ],
  providers: [ApiService, {
    provide: ErrorStateMatcher,
    useClass: ShowOnDirtyErrorStateMatcher
  },
    { provide: MAT_DATE_LOCALE, useValue: 'it' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

