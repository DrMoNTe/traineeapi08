export class Interface_op{
    op_id: string;
    c_name: string;
    op_name: string;
    status: string;
    res_1: string;
    res_2: string;
    sale: string;
    product: string;
    start_date: Date;
    budget: number;
    update_date: Date;
    action: string;
    target_date: Date;
    remark: string;


}