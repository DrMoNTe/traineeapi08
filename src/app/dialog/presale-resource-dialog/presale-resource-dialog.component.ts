import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Interface_Presale_Resource } from '../../interface/interface_presale_resource';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-presale-resource-dialog',
  templateUrl: './presale-resource-dialog.component.html',
  styleUrls: ['./presale-resource-dialog.component.scss']
})
export class PresaleResourceDialogComponent implements OnInit {

  ps_myForm: FormGroup;
  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  validation_messages = {
    'ps_name': [
      { type: 'required', message: 'Presale Resource Name is required' }
    ],
    'ps_position': [
      { type: 'required', message: 'Presale Resource Position is required' }
    ],
    'ps_name2': [
      { type: 'required', message: 'Presale Resource Name 2 is required' }
    ]
  };

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.ps_myForm = this.fb.group({
      ps_id: [''],
      ps_name: ['', Validators.required],
      ps_position: ['', Validators.required],
      ps_name2: ['', Validators.required],


    });
    this.ps_myForm = this.fb.group({
      ps_id: new FormControl({ value: null, disabled: true }),
      ps_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      ps_position: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      ps_name2: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))
    });
  }
  // CUSTOMER FUNCTION ------------------------------

  createPresaleResource(form) {

    console.log("FORM", form.value);
    this.apiService.checkPresaleResourceData(form.value.ps_name).subscribe(data => {
      console.log('DATA IN DATABASE', data);
      if (data.length == 0) {

        this.apiService.createPresaleResourceData(form.value).subscribe((Ipresale_resource: Interface_Presale_Resource) => {
          console.log("FORM", form.value);
          console.log("Presale Resource Created", Ipresale_resource);
          alert('Opportunity ADDED SUCCESS');
        });
      }
      else {
        alert('Data already exist inside Database\nPLEASE TRY AGAIN');
        console.log('Data already exist inside Database');
      }
     });
    }



}