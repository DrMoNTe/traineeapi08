import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresaleResourceDialogComponent } from './presale-resource-dialog.component';

describe('PresaleResourceDialogComponent', () => {
  let component: PresaleResourceDialogComponent;
  let fixture: ComponentFixture<PresaleResourceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresaleResourceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresaleResourceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
