import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Interface_Sale } from '../../interface/interface_sale';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-sale-dialog',
  templateUrl: './sale-dialog.component.html',
  styleUrls: ['./sale-dialog.component.scss']
})
export class SaleDialogComponent implements OnInit {

  am_myForm: FormGroup;
  validation_messages = {
    'am_name': [
      { type: 'required', message: 'Sale Name is required' }
    ],
    'am_email': [
      { type: 'required', message: 'Sale Email is required' },
      { type: 'email', message: 'Email is not Correct'}
    ]

  };

  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.am_myForm = this.fb.group({
      am_id: [''],
      am_name: ['', Validators.required],
      am_email: ['', Validators.required],

    });
    this.am_myForm = this.fb.group({
      am_id: new FormControl({ value: null, disabled: true }),
      am_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      am_email: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required,
        Validators.email
      ])),
    });
  }
  // Sale FUNCTION ------------------------------

  createSale(form) {


    console.log("FORM", form.value);
    this.apiService.checkSaleData(form.value.am_name).subscribe(data => {
      console.log('DATA IN DATABASE', data);
      if (data.length == 0) {
        this.apiService.createSaleData(form.value).subscribe((Isale: Interface_Sale) => {
          console.log("FORM", form.value);
          console.log("Sale Created", Isale);
          alert('Sale ADDED SUCCESS');
        });
      }
      else {
        alert('Data already exist inside Database\nPLEASE TRY AGAIN');
        console.log('Data already exist inside Database');
      }
    });




  }



}