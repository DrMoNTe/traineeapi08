import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Interface_Opportunity } from '../../interface/interface_opportunity';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-opportunity-dialog',
  templateUrl: './opportunity-dialog.component.html',
  styleUrls: ['./opportunity-dialog.component.scss']
})
export class OpportunityDialogComponent implements OnInit {

  op_myForm: FormGroup;
  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  validation_messages = {
    'op_name': [
      { type: 'required', message: 'Opportunity Name is required' }
    ]
  };

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.op_myForm = this.fb.group({
      op_id: [''],
      op_name: ['', Validators.required],


    });
    this.op_myForm = this.fb.group({
      op_id: new FormControl({ value: null, disabled: true }),
      op_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))
    });
  }
  // CUSTOMER FUNCTION ------------------------------

  createOpportunity(form) {

    console.log("FORM", form.value);
    this.apiService.checkOpportunityData(form.value.op_name).subscribe(data => {
      console.log('DATA IN DATABASE', data);
      if (data.length == 0) {
        this.apiService.createOpportunityData(form.value).subscribe((Iopportunity: Interface_Opportunity) => {
          console.log("FORM", form.value);
          console.log("Opportunity Created", Iopportunity);
          alert('Opportunity ADDED SUCCESS');
        });
      }
      else {
        alert('Data already exist inside Database\nPLEASE TRY AGAIN');
        console.log('Data already exist inside Database');
      }
    });




  }



}