import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpportunityDialogComponent } from './opportunity-dialog.component';

describe('OpportunityDialogComponent', () => {
  let component: OpportunityDialogComponent;
  let fixture: ComponentFixture<OpportunityDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpportunityDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpportunityDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
