import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Interface_Customer } from '../../interface/interface_customer';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-customer-dialog',
  templateUrl: './customer-dialog.component.html',
  styleUrls: ['./customer-dialog.component.scss']
})
export class CustomerDialogComponent implements OnInit {
  c_myForm: FormGroup;

  validation_messages = {
    'c_name': [
      { type: 'required', message: 'Customer Name is required' }
    ]
  };


  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.c_myForm = this.fb.group({
      c_id: [''],
      c_name: ['', Validators.required],
      c_sname: [''],

    });
    this.c_myForm = this.fb.group({
      c_id: new FormControl({ value: null, disabled: true }),
      c_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
      c_sname: new FormControl('', Validators.compose([
        Validators.maxLength(30),
      ])),

    });
  }
  // CUSTOMER FUNCTION ------------------------------

  createCustomer(form) {
    console.log("FORM", form.value);
    this.apiService.checkCustomerData(form.value.c_name).subscribe(data => {
      console.log('DATA IN DATABASE', data);
      if (data.length == 0) {
        this.apiService.createCustomerData(form.value).subscribe((Icustomer: Interface_Customer) => {
          console.log("FORM", form.value);
          console.log("Customer Created", Icustomer);
          alert('CUSTOMER ADDED SUCCESS');
        });
      }
      else {
        alert('Data already exist inside Database\nPLEASE TRY AGAIN');
        console.log('Data already exist inside Database');
      }
    });






  }


}
