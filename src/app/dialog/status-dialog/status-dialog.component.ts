import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Interface_Status } from '../../interface/interface_status';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-status-dialog',
  templateUrl: './status-dialog.component.html',
  styleUrls: ['./status-dialog.component.scss']
})
export class StatusDialogComponent implements OnInit {
  s_myForm: FormGroup;
  validation_messages = {
    's_name': [
      { type: 'required', message: 'Status Name is required' }
    ]
  };

  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.s_myForm = this.fb.group({
      s_id: [''],
      s_name: ['', Validators.required],


    });
    this.s_myForm = this.fb.group({
      s_id: new FormControl({ value: null, disabled: true }),
      s_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ]))
    });
  }
  // STATUS FUNCTION ------------------------------

  createStatus(form) {

    console.log("FORM", form.value);
    this.apiService.checkStatusData(form.value.s_name).subscribe(data => {
      console.log('DATA IN DATABASE', data);
      if (data.length == 0) {
        this.apiService.createStatusData(form.value).subscribe((Istatus: Interface_Status) => {
          console.log("FORM", form.value);
          console.log("Status Created", Istatus);
          alert('Status ADDED SUCCESS');
        });
      }
      else {
        alert('Data already exist inside Database\nPLEASE TRY AGAIN');
        console.log('Data already exist inside Database');
      }
    });



  }


}
