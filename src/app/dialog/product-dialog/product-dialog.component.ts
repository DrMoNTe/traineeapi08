import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Interface_Product } from '../../interface/interface_product';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {


  p_myForm: FormGroup;
  validation_messages = {
    'p_name': [
      { type: 'required', message: 'Product Name is required' }
    ]
  };
  constructor(private apiService: ApiService, private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.p_myForm = this.fb.group({
      p_id: [''],
      p_name: ['', Validators.required],

    });
    this.p_myForm = this.fb.group({
      p_id: new FormControl({ value: null, disabled: true }),
      p_name: new FormControl('', Validators.compose([
        Validators.maxLength(50),
        Validators.required
      ])),
    });
  }
  // PRODUCT FUNCTION ------------------------------

  createProduct(form) {

    console.log("FORM", form.value);
    this.apiService.checkProductData(form.value.p_name).subscribe(data => {
      console.log('DATA IN DATABASE', data);
      if (data.length == 0) {
        this.apiService.createProductData(form.value).subscribe((Iproduct: Interface_Product) => {
          console.log("FORM", form.value);
          console.log("Product Created", Iproduct);
          alert('PRODUCT ADDED SUCCESS');
        });
      }
      else {
        alert('Data already exist inside Database\nPLEASE TRY AGAIN');
        console.log('Data already exist inside Database');
      }
    });



  }



}