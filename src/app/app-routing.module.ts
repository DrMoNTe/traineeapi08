import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { ManageDataComponent } from './manage-data/manage-data.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'data', component: ManageDataComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
