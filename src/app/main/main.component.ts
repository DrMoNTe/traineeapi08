import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Interface_op } from '../interface/interface_op';
import { Interface_Customer } from '../interface/interface_customer';
import { Interface_Opportunity } from '../interface/interface_opportunity';
import { Interface_Presale_Resource } from '../interface/interface_presale_resource';
import { Interface_Product } from '../interface/interface_product';
import { Interface_Sale } from '../interface/interface_sale';
import { Interface_Status } from '../interface/interface_status';
import { Interface_Type } from '../interface/interface_type';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { MatSort } from '@angular/material/sort';
import * as moment from 'moment';
import { CustomerDialogComponent } from '../dialog/customer-dialog/customer-dialog.component';
import { OpportunityDialogComponent } from '../dialog/opportunity-dialog/opportunity-dialog.component';
import { StatusDialogComponent } from '../dialog/status-dialog/status-dialog.component';
import { PresaleResourceDialogComponent } from '../dialog/presale-resource-dialog/presale-resource-dialog.component';
import { SaleDialogComponent } from '../dialog/sale-dialog/sale-dialog.component';
import { ProductDialogComponent } from '../dialog/product-dialog/product-dialog.component';


import { Observable, of, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('c_input') cinput:ElementRef;

  validation_messages = {
    'c_name': [
      { type: 'required', message: 'Customer Name is required / Please select a option' },
    ],
    'op_name': [
      { type: 'required', message: 'Opportunity is required / Please select a option' }
    ],
    'status': [
      { type: 'required', message: 'Status is required / Please select a option' }
    ],
    'res_1': [
      { type: 'required', message: 'Resource 1 is required / Please select a option' }
    ],
    /*     'res_2': [
          { type: 'required', message: 'Resource 2 is required' }
        ], */
    'sale': [
      { type: 'required', message: 'Sale is required  / Please select a option' }
    ],
    'product': [
      { type: 'required', message: 'Product is required  / Please select a option' }
    ],
    'start_date': [
      { type: 'required', message: 'Start Date is required' },
      { type: 'type', message: 'Date incorrect' }
    ],
    'budget': [
      { type: 'required', message: 'Budget is required' },
      { type: 'min', message: 'Budget incorrect' }
    ],
    'update_date': [
      { type: 'required', message: 'Update Date is required' },
      { type: 'type', message: 'Date incorrect' }
    ],
    /*     'action': [
          { type: 'required', message: 'Activity & Next Action is required' }
        ], */
    'targer_date': [
      { type: 'required', message: 'Close Target Date is required' },
      { type: 'type', message: 'Date incorrect' }
    ],
    /*     'remark': [
          { type: 'required', message: 'Remark is required' }
        ] */

  };

  myForm: FormGroup;
  delayTime = 300;
  Iop: Interface_op[];
  Icustomer: Interface_Customer[];
  Iopportunity: Interface_Opportunity[];
  Ipresale_resource: Interface_Presale_Resource[];
  Iproduct: Interface_Product[];
  Isale: Interface_Sale[];
  Istatus: Interface_Status[];
  Itype: Interface_Type[];
  displayedColumns: string[] = ['c_name', 'op_name', 'status', 'res_1', 'res_2', 'sale', 'product', 'start_date', 'budget', 'update_date', 'action', 'target_date', 'remark']
  displayedColumns2 = {
    column_name: [
      { key: 'c_name', value: 'Customer Name' },
      { key: 'op_name', value: 'Opportunity Name' },
      { key: 'status', value: 'Status' },
      { key: 'res_1', value: 'Resource 1' },
      { key: 'res_2', value: 'Resource 2' },
      { key: 'sale', value: 'Sale' },
      { key: 'product', value: 'Product' },
      { key: 'start_date', value: 'Start Date' },
      { key: 'budget', value: 'Budget' },
      { key: 'update_date', value: 'Update Date' },
      { key: 'action', value: 'Action' },
      { key: 'target_date', value: 'Target_Date' },
      { key: 'remark', value: 'Remark' }
    ]
  }

  FilterData;
  selecting = new FormControl(['op_id', 'c_name', 'op_name', 'status', 'res_1', 'res_2', 'sale', 'product', 'start_date', 'budget', 'update_date', 'action', 'target_date', 'remark', 'option']);


  /* ---------INPUT ENV----- */
  c_selectList: Observable<Interface_Customer[]>
  op_selectList: Observable<Interface_Opportunity[]>
  s_selectList: Observable<Interface_Status[]>
  ps_selectList: Observable<Interface_Presale_Resource[]>
  ps2_selectList: Observable<Interface_Presale_Resource[]>
  am_selectList: Observable<Interface_Sale[]>
  p_selectList: Observable<Interface_Product[]>
  private customerSearchSubject = new Subject<string>();
  private opportunitySearchSubject = new Subject<string>();
  private statusSearchSubject = new Subject<string>();
  private presale_resourceSearchSubject = new Subject<string>();
  
  private saleSearchSubject = new Subject<string>();
  private productSearchSubject = new Subject<string>();


  constructor(private apiService: ApiService, private fb: FormBuilder, public dialog: MatDialog) {

  }

  async delay(ms: number) {
    await new Promise(resolve => setTimeout(() => resolve(), ms)).then(() => console.log("fired"));
  }

  ngOnInit() {
    this.loadingData();

    this.createForms();
    

    this.initLookup();
  

    

  }
  checkValue(form){
    if(!this.myForm.get(form).value){
      this.myForm.get(form).setValue('');
      console.log('Clear Input Value',this.myForm.get(form).value);
    }
    
  }
  setINITValue(){
    this.myForm.get('op_id').reset();
    this.myForm.get('c_name').reset();
    this.myForm.get('op_name').reset();
    this.myForm.get('status').reset();
    this.myForm.get('res_1').reset();
    this.myForm.get('res_2').reset();
    this.myForm.get('sale').reset();
    this.myForm.get('product').reset();
    this.myForm.get('start_date').reset();
    this.myForm.get('budget').reset();
    this.myForm.get('update_date').reset();
    this.myForm.get('action').reset();
    this.myForm.get('target_date').reset();
    this.myForm.get('remark').reset();

  }

  getCustomerDisplayValue(Icustomer?: Interface_Customer): string | undefined {
    return Icustomer && Icustomer.c_name || undefined;
}

  /* ------------AUTO COMPLETE INPUT--------------- */
  searchCustomer(text: string) {
    this.customerSearchSubject.next(text);

  }
  searchOpportunity(text: string) {
    this.opportunitySearchSubject.next(text);
  }
  searchStatus(text: string) {
    this.statusSearchSubject.next(text);
  }
  searchPresaleResource(text: string) {
    this.presale_resourceSearchSubject.next(text);
  }
  searchSale(text: string) {
    this.saleSearchSubject.next(text);
  }
  searchProduct(text: string) {
    this.productSearchSubject.next(text);
  }

  private initLookup() {
    this.c_selectList = this.customerSearchSubject
      .pipe(
        map(text => this.getCustomer(text))
      );
      

    this.op_selectList = this.opportunitySearchSubject
      .pipe(
        map(text => this.getOpportunity(text))
      );
    this.s_selectList = this.statusSearchSubject
      .pipe(
        map(text => this.getStatus(text))
      );
    this.ps_selectList = this.presale_resourceSearchSubject
      .pipe(
        map(text => this.getPresaleResource(text))
      );
    this.ps2_selectList = this.presale_resourceSearchSubject
      .pipe(
        map(text => this.getPresaleResource(text))
      );
    this.am_selectList = this.saleSearchSubject
      .pipe(
        map(text => this.getSale(text))
      );
    this.p_selectList = this.productSearchSubject
      .pipe(
        map(text => this.getProduct(text))
      );
  }

  private getCustomer(text: string): Interface_Customer[] {
    let expression = new RegExp(`\\b${text}`, "i");
    return this.Icustomer
      .sort((a, b) => a.c_name.toLowerCase().localeCompare(b.c_name.toLowerCase()))
      .filter(data => expression.test(data.c_name))
      .slice(0, 100);
  }
  private getOpportunity(text: string): Interface_Opportunity[] {
    let expression = new RegExp(`\\b${text}`, "i");
    return this.Iopportunity
      .sort((a, b) => a.op_name.toLowerCase().localeCompare(b.op_name.toLowerCase()))
      .filter(data => expression.test(data.op_name))
      .slice(0, 100);
  }
  private getStatus(text: string): Interface_Status[] {
    let expression = new RegExp(`\\b${text}`, "i");
    return this.Istatus
      .sort((a, b) => a.s_name.toLowerCase().localeCompare(b.s_name.toLowerCase()))
      .filter(data => expression.test(data.s_name))
      .slice(0, 100);
  }
  private getPresaleResource(text: string): Interface_Presale_Resource[] {
    let expression = new RegExp(`\\b${text}`, "i");
    return this.Ipresale_resource
      .sort((a, b) => a.ps_name.toLowerCase().localeCompare(b.ps_name.toLowerCase()))
      .filter(data => expression.test(data.ps_name))
      .slice(0, 100);
  }
  private getSale(text: string): Interface_Sale[] {
    let expression = new RegExp(`\\b${text}`, "i");
    return this.Isale
      .sort((a, b) => a.am_name.toLowerCase().localeCompare(b.am_name.toLowerCase()))
      .filter(data => expression.test(data.am_name))
      .slice(0, 100);
  }
  private getProduct(text: string): Interface_Product[] {
    let expression = new RegExp(`\\b${text}`, "i");
    return this.Iproduct
      .sort((a, b) => a.p_name.toLowerCase().localeCompare(b.p_name.toLowerCase()))
      .filter(data => expression.test(data.p_name))
      .slice(0, 100);
  }

  /* --------Dialog---------- */
  openAddCustomerDialog(): void {
    const dialogRef = this.dialog.open(CustomerDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog ws closed");
      console.log(result);
      this.delay(this.delayTime).then(any => {
        this.apiService.readCustomerData().subscribe((Icustomer: Interface_Customer[]) => {
          this.Icustomer = Icustomer;
          console.log("CUSTOMER", this.Icustomer);
        });
      });
    })

  }

  openAddOpportunityDialog(): void {
    const dialogRef = this.dialog.open(OpportunityDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog ws closed");
      console.log(result);
      this.delay(this.delayTime).then(any => {
        this.apiService.readOpportunityData().subscribe((Iopportunity: Interface_Opportunity[]) => {
          this.Iopportunity = Iopportunity;
          console.log("OPPORTUNITY", this.Iopportunity);
        });
      });
    })
  }

  openAddStatusDialog(): void {
    const dialogRef = this.dialog.open(StatusDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog ws closed");
      console.log(result);
      this.delay(this.delayTime).then(any => {
        this.apiService.readStatusData().subscribe((Istatus: Interface_Status[]) => {
          this.Istatus = Istatus;
          console.log("STATUS", this.Istatus);
        });
      });
    })
  }
  openAddPresaleResourceDialog(): void {
    const dialogRef = this.dialog.open(PresaleResourceDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog ws closed");
      console.log(result);
      this.delay(this.delayTime).then(any => {
        this.apiService.readPresaleResourceData().subscribe((Ipresale_resource: Interface_Presale_Resource[]) => {
          this.Ipresale_resource = Ipresale_resource;
          console.log("PRESALE RESOURCE", this.Ipresale_resource);
        });
      });
    })
  }
  openAddSaleDialog(): void {
    const dialogRef = this.dialog.open(SaleDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog ws closed");
      console.log(result);
      this.delay(this.delayTime).then(any => {
        this.apiService.readSaleData().subscribe((Isale: Interface_Sale[]) => {
          this.Isale = Isale;
          console.log("SALE", this.Isale);
        });
      });
    })
  }

  openAddProductDialog(): void {
    const dialogRef = this.dialog.open(ProductDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      console.log("The dialog ws closed");
      console.log(result);
      this.delay(this.delayTime).then(any => {
        this.apiService.readProductData().subscribe((Iproduct: Interface_Product[]) => {
          this.Iproduct = Iproduct;
          console.log("PRODUCT", this.Iproduct);
        });
      });
    })
  }



  loadingData() {
    this.delay(this.delayTime).then(any => {
      this.apiService.readOpData().subscribe((Iop: Interface_op[]) => {
        this.Iop = Iop;
        this.FilterData = new MatTableDataSource(this.Iop);
        this.FilterData.sort = this.sort;
        console.log("IOP", this.Iop);
        console.log("Filter", this.FilterData.filteredData);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readCustomerData().subscribe((Icustomer: Interface_Customer[]) => {
        this.Icustomer = Icustomer;
      
        console.log("CUSTOMER", this.Icustomer);
      });
    });

    this.delay(this.delayTime).then(any => {
      this.apiService.readOpportunityData().subscribe((Iopportunity: Interface_Opportunity[]) => {
        this.Iopportunity = Iopportunity;
        console.log(this.Iopportunity);
      });
      this.c_selectList = this.customerSearchSubject
      .pipe(
        map(text => this.getCustomer(text))
      );
    });

    this.delay(this.delayTime).then(any => {
      this.apiService.readPresaleResourceData().subscribe((Ipresale_resource: Interface_Presale_Resource[]) => {
        this.Ipresale_resource = Ipresale_resource;
        console.log(this.Ipresale_resource);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readProductData().subscribe((Iproduct: Interface_Product[]) => {
        this.Iproduct = Iproduct;
        console.log(this.Iproduct);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readSaleData().subscribe((Isale: Interface_Sale[]) => {
        this.Isale = Isale;
        console.log(this.Isale);
      });
    });

    this.delay(this.delayTime).then(any => {
      this.apiService.readStatusData().subscribe((Istatus: Interface_Status[]) => {
        this.Istatus = Istatus;
        console.log(this.Istatus);
      });
    });
    this.delay(this.delayTime).then(any => {
      this.apiService.readTypeData().subscribe((Itype: Interface_Type[]) => {
        this.Itype = Itype;

        console.log(this.Itype);
      });
    });



  }
  applyFilter(filterValue) {
    this.FilterData.filter = filterValue.trim().toLowerCase();
    console.log("FILTER WITH ", this.FilterData.filter, this.FilterData.filteredData);
  }


  createForms() {

    this.myForm = this.fb.group({
      op_id: [''],
      c_name: ['', Validators.required],
      op_name: ['', Validators.required],
      status: ['', Validators.required],
      res_1: ['', Validators.required],
      res_2: [''],
      sale: ['', Validators.required],
      product: ['', Validators.required],
      start_date: [moment(), Validators.required],
      budget: [0, Validators.required],
      update_date: [moment(), Validators.required],
      action: [''],
      target_date: [moment(), Validators.required],
      remark: [''],
      forceSelection: true

    });
    this.myForm = this.fb.group({
      op_id: new FormControl({ value: null, disabled: true }, Validators.compose([
        Validators.min(1),
      ])),
      c_name: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.required,
      ])),
      op_name: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])),
      status: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])),
      res_1: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])),
      res_2: new FormControl(''),
      sale: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])),
      product: new FormControl('', Validators.compose([
        Validators.minLength(1),
        Validators.required
      ])),
      start_date: new FormControl(moment(), Validators.compose([
        Validators.required,
        
      ])),
      budget: new FormControl(0, Validators.compose([
        Validators.min(0),
        Validators.required
      ])),
      update_date: new FormControl(moment(), Validators.compose([
        Validators.required
      ])),
      action: new FormControl(''),
      target_date: new FormControl(moment(), Validators.compose([
        Validators.required
      ])),
      remark: new FormControl(''),
      forceSelection: true
    });
    console.log("FORM CREATED");



  }


  /* ------------CRUD-------------- */

  createOrUpdateOpportunity(form) {

    /*  if (form.value.c_name in this.Icustomer && form.value.op_name in this.Iopportunity) { */
    console.log("PASSSSSSSSSSSSSSSSSS");
    console.log("FORM---", form.getRawValue());
    if (form.getRawValue().op_id) {
      this.apiService.updateOpData(form.getRawValue()).subscribe((Iop: Interface_op) => {
        console.log("Opportunity Updated",Iop);
        alert('UPDATE SUCCESS');
      });

    }
    else {
      this.apiService.createOpData(form.value).subscribe((Iop: Interface_op) => {
        console.log("Opportunity Created",Iop);
        alert('CREATE SUCCESS')
      });

    }
    this.loadingData();
    this.setINITValue();

    /*     }
        else{
          console.log("ERRORRRRRR")
        } */



  }
  selectOpportunity(Iop: Interface_op) {
    this.myForm.get('op_id').setValue(Iop.op_id);
    this.myForm.get('c_name').setValue(Iop.c_name);
    this.myForm.get('op_name').setValue(Iop.op_name);
    this.myForm.get('status').setValue(Iop.status);
    this.myForm.get('res_1').setValue(Iop.res_1);
    this.myForm.get('res_2').setValue(Iop.res_2);
    this.myForm.get('sale').setValue(Iop.sale);
    this.myForm.get('product').setValue(Iop.product);
    this.myForm.get('start_date').setValue(moment(Iop.start_date, "DD-MM-YYYY").format('YYYY-MM-DD'));
    this.myForm.get('budget').setValue(Iop.budget);
    this.myForm.get('update_date').setValue(moment(Iop.update_date, "DD-MM-YYYY").format('YYYY-MM-DD'));
    this.myForm.get('action').setValue(Iop.action);
    this.myForm.get('target_date').setValue(moment(Iop.target_date, "DD-MM-YYYY").format('YYYY-MM-DD'));
    this.myForm.get('remark').setValue(Iop.remark);
    this.myForm.get('forceSelection').setValue(true);

    console.log("Opportunity Selected", this.myForm.getRawValue());
  }

  deleteOpportunity(id) {
    this.apiService.deleteOpData(id).subscribe((Iop: Interface_op) => {
      console.log("Opportunity Deleted", Iop);
      this.loadingData();
      alert('DELETE SUCCESS');
    });

  }



}

