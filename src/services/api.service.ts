import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Interface_op } from '../app/interface/interface_op';
import { Interface_Customer } from '../app/interface/interface_customer';
import { Interface_Opportunity } from '../app/interface/interface_opportunity';
import { Interface_Presale_Resource } from '../app/interface/interface_presale_resource';
import { Interface_Product } from '../app/interface/interface_product';
import { Interface_Sale } from '../app/interface/interface_sale';
import { Interface_Status } from '../app/interface/interface_status';
import { Interface_Type } from '../app/interface/interface_type';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  NODE_API_SERVER = "http://localhost:3003";

  // MAIN TABLE------------------

  readOpData(): Observable<Interface_op[]> {
    return this.httpClient.get<Interface_op[]>(`${this.NODE_API_SERVER}/main`);
  }
  createOpData(Iop: Interface_op): Observable<Interface_op> {
    return this.httpClient.post<Interface_op>(`${this.NODE_API_SERVER}/main`, Iop);
  }
  updateOpData(Iop: Interface_op) {
    return this.httpClient.put<Interface_op>(`${this.NODE_API_SERVER}/main`, Iop);
  }
  deleteOpData(id: number) {
    return this.httpClient.delete<Interface_op>(`${this.NODE_API_SERVER}/main/${id}`);
  }

  // CUSTOMER TABLE ---------------

  readCustomerData(): Observable<Interface_Customer[]> {
    return this.httpClient.get<Interface_Customer[]>(`${this.NODE_API_SERVER}/customer`);
  }
  checkCustomerData(name: string): Observable<Interface_Customer[]> {
    return this.httpClient.get<Interface_Customer[]>(`${this.NODE_API_SERVER}/customer/${name}`);
  }
  createCustomerData(Icustomer: Interface_Customer): Observable<Interface_Customer> {
    return this.httpClient.post<Interface_Customer>(`${this.NODE_API_SERVER}/customer`, Icustomer);
  }
  updateCustomerData(Icustomer: Interface_Customer) {
    return this.httpClient.put<Interface_Customer>(`${this.NODE_API_SERVER}/customer`, Icustomer);
  }
  deleteCustomerData(id: number) {
    return this.httpClient.delete<Interface_Customer>(`${this.NODE_API_SERVER}/customer/${id}`);
  }

  // Opportunity Table

  readOpportunityData(): Observable<Interface_Opportunity[]> {
    return this.httpClient.get<Interface_Opportunity[]>(`${this.NODE_API_SERVER}/opportunity`);
  }
  checkOpportunityData(name: string):Observable<Interface_Opportunity[]> {
    return this.httpClient.get<Interface_Opportunity[]>(`${this.NODE_API_SERVER}/opportunity/${name}`);
  }
  createOpportunityData(Iopportunity: Interface_Opportunity): Observable<Interface_Opportunity> {
    return this.httpClient.post<Interface_Opportunity>(`${this.NODE_API_SERVER}/opportunity`, Iopportunity);
  }
  updateOpportunityData(Iopportunity: Interface_Opportunity) {
    return this.httpClient.put<Interface_Opportunity>(`${this.NODE_API_SERVER}/opportunity`, Iopportunity);
  }
  deleteOpportunityData(id: string) {
    return this.httpClient.delete<Interface_Opportunity>(`${this.NODE_API_SERVER}/opportunity/${id}`);
  }

  // Presale Resource Table

  readPresaleResourceData(): Observable<Interface_Presale_Resource[]> {
    return this.httpClient.get<Interface_Presale_Resource[]>(`${this.NODE_API_SERVER}/presale_resource`);
  }
  checkPresaleResourceData(name: string): Observable<Interface_Presale_Resource[]> {
    return this.httpClient.get<Interface_Presale_Resource[]>(`${this.NODE_API_SERVER}/presale_resource/${name}`);
  }
  createPresaleResourceData(Ipresale_resource: Interface_Presale_Resource): Observable<Interface_Presale_Resource> {
    return this.httpClient.post<Interface_Presale_Resource>(`${this.NODE_API_SERVER}/presale_resource`, Ipresale_resource);
  }
  updatePresaleResourceData(Ipresale_resource: Interface_Presale_Resource) {
    return this.httpClient.put<Interface_Presale_Resource>(`${this.NODE_API_SERVER}/presale_resource`, Ipresale_resource);
  }
  deletePresaleResourceData(id: string) {
    return this.httpClient.delete<Interface_Presale_Resource>(`${this.NODE_API_SERVER}/presale_resource/${id}`);
  }


  // Product Table

  readProductData(): Observable<Interface_Product[]> {
    return this.httpClient.get<Interface_Product[]>(`${this.NODE_API_SERVER}/product`);
  }
  checkProductData(name: string): Observable<Interface_Product[]> {
    return this.httpClient.get<Interface_Product[]>(`${this.NODE_API_SERVER}/product/${name}`);
  }
  createProductData(Iproduct: Interface_Product): Observable<Interface_Product> {
    return this.httpClient.post<Interface_Product>(`${this.NODE_API_SERVER}/product`, Iproduct);
  }
  updateProductData(Iproduct: Interface_Product) {
    return this.httpClient.put<Interface_Product>(`${this.NODE_API_SERVER}/product`, Iproduct);
  }
  deleteProductData(id: string) {
    return this.httpClient.delete<Interface_Product>(`${this.NODE_API_SERVER}/product/${id}`);
  }

  // Sale Table

  readSaleData(): Observable<Interface_Sale[]> {
    return this.httpClient.get<Interface_Sale[]>(`${this.NODE_API_SERVER}/sale`);
  }
  checkSaleData(name: string): Observable<Interface_Sale[]> {
    return this.httpClient.get<Interface_Sale[]>(`${this.NODE_API_SERVER}/sale/${name}`);
  }
  createSaleData(Isale: Interface_Sale): Observable<Interface_Sale> {
    return this.httpClient.post<Interface_Sale>(`${this.NODE_API_SERVER}/sale`, Isale);
  }
  updateSaleData(Isale: Interface_Sale) {
    return this.httpClient.put<Interface_Sale>(`${this.NODE_API_SERVER}/sale`, Isale);
  }
  deleteSaleData(id: string) {
    return this.httpClient.delete<Interface_Sale>(`${this.NODE_API_SERVER}/sale/${id}`);
  }

  // Status Table

  readStatusData(): Observable<Interface_Status[]> {
    return this.httpClient.get<Interface_Status[]>(`${this.NODE_API_SERVER}/status`);
  }
  checkStatusData(name: string): Observable<Interface_Status[]> {
    return this.httpClient.get<Interface_Status[]>(`${this.NODE_API_SERVER}/status/${name}`);
  }
  createStatusData(Istatus: Interface_Status): Observable<Interface_Status> {
    return this.httpClient.post<Interface_Status>(`${this.NODE_API_SERVER}/status`, Istatus);
  }
  updateStatusData(Istatus: Interface_Status) {
    return this.httpClient.put<Interface_Status>(`${this.NODE_API_SERVER}/status`, Istatus);
  }
  deleteStatusData(id: string) {
    return this.httpClient.delete<Interface_Status>(`${this.NODE_API_SERVER}/status/${id}`);
  }

  // Type Table

  readTypeData(): Observable<Interface_Type[]> {
    return this.httpClient.get<Interface_Type[]>(`${this.NODE_API_SERVER}/type`);
  }
  checkTypeData(name: string): Observable<Interface_Type[]> {
    return this.httpClient.get<Interface_Type[]>(`${this.NODE_API_SERVER}/type/${name}`);
  }

  createTypeData(Itype: Interface_Type): Observable<Interface_Type> {
    return this.httpClient.post<Interface_Type>(`${this.NODE_API_SERVER}/type`, Itype);
  }
  updateTypeData(Itype: Interface_Type) {
    return this.httpClient.put<Interface_Type>(`${this.NODE_API_SERVER}/type`, Itype);
  }
  deleteTypeData(id: string) {
    return this.httpClient.delete<Interface_Type>(`${this.NODE_API_SERVER}/type/${id}`);
  }











}

